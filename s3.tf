#Deploy files to S3 Bucket. Loops through all files in www folder
resource "aws_s3_bucket_object" "files" {
  provider         = aws.main
  for_each         = fileset("./www/", "*")
  bucket           = module.main.s3_bucket_id
  content_type     = "text/html"
  acl              = "public-read"
  key              = each.value
  source           = "./www/${each.value}"
  force_destroy    = true
  # etag makes the file update when it changes;
  etag             = filemd5("./www/${each.value}")
}