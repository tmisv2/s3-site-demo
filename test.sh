#!/bin/bash
#sleep for 30 seconds to allow for any delay
sleep 30
# Test if hitting site returns 200 OK
if curl -s --head  --request GET https://s3demo.tom-currie.co.uk | grep "200" > /dev/null; then
   echo "Success: site is UP"
else
   echo "Error: Site is DOWN"
fi