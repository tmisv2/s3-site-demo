#Store statefile in S3
terraform {
  backend "s3" {
    bucket = "tmisv2-statefiles"
    key    = "s3-demo/terraform.tfstate"
    region = "us-east-1"
  }
}

