# Variables
variable "fqdn" {
  description = "The fully-qualified domain name of the resulting S3 website."
}

variable "domain" {
  description = "The domain name."
}

# Allowed IPs that can directly access the S3 bucket, this can be used to secure the s3 Bucket if necessary. Files are +rx only anyway
variable "allowed_ips" {
  type = list
  default = [
    "0.0.0.0/0"            # public access
    # "xxx.xxx.xxx.xxx/mm" # restricted
    # "999.999.999.999/32" # invalid IP, completely inaccessible
  ]
}